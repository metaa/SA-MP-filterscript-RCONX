#include <a_samp>

#pragma tabsize 0
#define VERSION "0.1"

/*######################################*\
##										##
##  		RCONX v0.1 by Meta	 		##
##										##
\*######################################*/

public OnPlayerCommandText(playerid, cmdtext[])
{
    new idx, cmd[128], tmp[128]; cmd = strtok(cmdtext, idx);
    if(!strcmp(cmd, "/rconX", true))
    {
        tmp = strtok(cmdtext, idx);
        if(strlen(tmp))
        {
			if(!strcmp(tmp, "login", true))
			{
			    if(GetPVarInt(playerid, "RCONExLogin") || IsPlayerAdmin(playerid)) { return SendClientMessage(playerid, 0xFFFFFFFF, "SERVER: You already are logged in as admin."); }
			    cmd = strtok(cmdtext, idx);
			    GetServerVarAsString("rcon_password", tmp, sizeof(tmp));
				if(strlen(cmd))
				{
					if(strcmp(tmp, cmd, true))
					{
					    SendClientMessage(playerid, 0xFFFFFFFF, "SERVER: Bad admin password. Repeated attempts will get you banned.");
					}
					else
					{
					    SendClientMessage(playerid, 0xFFFFFFFF, "SERVER: You are logged in as admin.");
					    SetPVarInt(playerid, "RCONExLogin", 1);
					}
				}
			}
			if(!GetPVarInt(playerid, "RCONExLogin")) { return 1; }
			if(!strcmp(tmp, "logout", true))
			{
			    SendClientMessage(playerid, 0xFFFFFFFF, "SERVER: You logged out. Bye.");
			    SetPVarInt(playerid, "RCONExLogin", 0);
			}
			else if(!strcmp(tmp, "echo", true))
			{
				cmd = strtok(cmdtext, idx);
				if(strlen(cmd))
				{
					SendClientMessage(playerid, 0xFFFFFFFF, cmd);
					format(cmd, sizeof(cmd), "echo %s", cmd);
					SendRconCommand(cmd);
    			}
			}
	        else if(!strcmp(tmp, "cmdlist", true))
	        {
	            SendClientMessage(playerid, 0xFFFFFFFF, "Console Commands:");
				SendClientMessage(playerid, 0xFFFFFFFF, "  echo");
				SendClientMessage(playerid, 0xFFFFFFFF, "  exec");
				SendClientMessage(playerid, 0xFFFFFFFF, "  cmdlist");
				SendClientMessage(playerid, 0xFFFFFFFF, "  varlist");
				SendClientMessage(playerid, 0xFFFFFFFF, "  exit");
				SendClientMessage(playerid, 0xFFFFFFFF, "  kick");
				SendClientMessage(playerid, 0xFFFFFFFF, "  ban");
				SendClientMessage(playerid, 0xFFFFFFFF, "  gmx");
				SendClientMessage(playerid, 0xFFFFFFFF, "  changemode");
				SendClientMessage(playerid, 0xFFFFFFFF, "  say");
				SendClientMessage(playerid, 0xFFFFFFFF, "  reloadbans");
				SendClientMessage(playerid, 0xFFFFFFFF, "  reloadlog");
				SendClientMessage(playerid, 0xFFFFFFFF, "  players");
				SendClientMessage(playerid, 0xFFFFFFFF, "  banip");
				SendClientMessage(playerid, 0xFFFFFFFF, "  unbanip");
				SendClientMessage(playerid, 0xFFFFFFFF, "  gravity");
				SendClientMessage(playerid, 0xFFFFFFFF, "  weather");
				SendClientMessage(playerid, 0xFFFFFFFF, "  loadfs");
				SendClientMessage(playerid, 0xFFFFFFFF, "  unloadfs");
				SendClientMessage(playerid, 0xFFFFFFFF, "  reloadfs");
			}
			else if(!strcmp(tmp, "varlist", true))
			{
			    SendClientMessage(playerid, 0xFFFFFFFF, "Console Variables:");
																			format(tmp, sizeof(tmp), "  announce      = %d  (bool)", GetServerVarAsBool("announce"));					SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("bind", tmp, sizeof(tmp));				format(tmp, sizeof(tmp), "  bind          = \"%s\"  (string) (read-only)", tmp);							SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("filterscripts", tmp, sizeof(tmp)); 	format(tmp, sizeof(tmp), "  filterscripts = \"%s\"  (string) (read-only)", tmp);							SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("gamemode0", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "  gamemode0     = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("gamemode1", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "  gamemode1     = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("gamemode10", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "  gamemode10    = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("gamemode11", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "  gamemode11    = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("gamemode12", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "  gamemode12    = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("gamemode13", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "  gamemode13    = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("gamemode14", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "  gamemode14    = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("gamemode15", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "  gamemode15    = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("gamemode2", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "  gamemode2     = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("gamemode3", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "  gamemode3     = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("gamemode4", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "  gamemode4     = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("gamemode5", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "  gamemode5     = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("gamemode6", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "  gamemode6     = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("gamemode7", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "  gamemode7     = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("gamemode8", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "  gamemode8     = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("gamemode9", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "  gamemode9     = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("gamemodetext", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "  gamemodetext  = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("gravity", tmp, sizeof(tmp));			format(tmp, sizeof(tmp), "  gravity       = \"%s\"  (string) (rule)", tmp);									SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("hostname", tmp, sizeof(tmp));			format(tmp, sizeof(tmp), "  hostname      = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
																			format(tmp, sizeof(tmp), "  incar_rate    = %d  (int) (read-only)", GetServerVarAsInt("incar_rate"));		SendClientMessage(playerid, 0xFFFFFFFF, tmp);
																			format(tmp, sizeof(tmp), "  lanmode       = %d  (bool)", GetServerVarAsBool("lanmode"));					SendClientMessage(playerid, 0xFFFFFFFF, tmp);
																			format(tmp, sizeof(tmp), "  logqueries    = %d  (bool)", GetServerVarAsBool("logqueries"));					SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("logtimeformat", tmp, sizeof(tmp)); strrep(tmp, "%", "%%");	format(tmp, sizeof(tmp), "  logtimeformat = \"%s\"  (string) (read-only)", tmp);	SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("mapname", tmp, sizeof(tmp));			format(tmp, sizeof(tmp), "  mapname       = \"%s\"  (string) (rule)", tmp);									SendClientMessage(playerid, 0xFFFFFFFF, tmp);
																			format(tmp, sizeof(tmp), "  maxnpc        = %d  (int)", GetServerVarAsInt("maxnpc"));						SendClientMessage(playerid, 0xFFFFFFFF, tmp);
																			format(tmp, sizeof(tmp), "  maxplayers    = %d  (int) (read-only)", GetServerVarAsInt("maxplayers"));		SendClientMessage(playerid, 0xFFFFFFFF, tmp);
																			format(tmp, sizeof(tmp), "  myriad        = %d  (bool)", GetServerVarAsBool("myriad"));						SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("nosign", tmp, sizeof(tmp));			format(tmp, sizeof(tmp), "  nosign        = \"%s\"  (string) (read-only)", tmp);							SendClientMessage(playerid, 0xFFFFFFFF, tmp);
																			format(tmp, sizeof(tmp), "  onfoot_rate   = %d  (int) (read-only)", GetServerVarAsInt("onfoot_rate"));		SendClientMessage(playerid, 0xFFFFFFFF, tmp);
																			format(tmp, sizeof(tmp), "  output        = %d  (bool)", GetServerVarAsBool("output"));						SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("password", tmp, sizeof(tmp));			format(tmp, sizeof(tmp), "  password      = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("plugins", tmp, sizeof(tmp));			format(tmp, sizeof(tmp), "  plugins       = \"%s\"  (string) (read-only)", tmp);							SendClientMessage(playerid, 0xFFFFFFFF, tmp);
																			format(tmp, sizeof(tmp), "  port          = %d  (int) (read-only)", GetServerVarAsInt("port"));				SendClientMessage(playerid, 0xFFFFFFFF, tmp);
																			format(tmp, sizeof(tmp), "  query         = %d  (bool)", GetServerVarAsBool("query"));						SendClientMessage(playerid, 0xFFFFFFFF, tmp);
																			format(tmp, sizeof(tmp), "  rcon          = %d  (bool)", GetServerVarAsBool("rcon"));						SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("rcon_password", tmp, sizeof(tmp));	format(tmp, sizeof(tmp), "  rcon_password = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
																			format(tmp, sizeof(tmp), "  sleep         = %d  (int)", GetServerVarAsInt("sleep"));						SendClientMessage(playerid, 0xFFFFFFFF, tmp);
																			format(tmp, sizeof(tmp), "  stream_distance       = %d  (float)", GetServerVarAsInt("stream_distance"));	SendClientMessage(playerid, 0xFFFFFFFF, tmp);
																			format(tmp, sizeof(tmp), "  stream_rate   = %d  (int)", GetServerVarAsInt("stream_rate"));					SendClientMessage(playerid, 0xFFFFFFFF, tmp);
																			format(tmp, sizeof(tmp), "  timestamp     = %d  (bool)", GetServerVarAsBool("timestamp"));					SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("version", tmp, sizeof(tmp));			format(tmp, sizeof(tmp), "  version       = \"%s\"  (string) (read-only) (rule)", tmp);						SendClientMessage(playerid, 0xFFFFFFFF, tmp);
																			format(tmp, sizeof(tmp), "  weapon_rate   = %d  (int) (read-only)", GetServerVarAsInt("weapon_rate"));		SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("weather", tmp, sizeof(tmp));			format(tmp, sizeof(tmp), "  weather       = \"%s\"  (string) (rule)", tmp);									SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("weburl", tmp, sizeof(tmp));			format(tmp, sizeof(tmp), "  weburl        = \"%s\"  (string) (rule)", tmp);									SendClientMessage(playerid, 0xFFFFFFFF, tmp);
				GetServerVarAsString("worldtime", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "  worldtime     = \"%s\"  (string) (rule)", tmp);									SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "exit", true) || !strcmp(tmp, "gmx", true) || !strcmp(tmp, "reloadbans", true) || !strcmp(tmp, "reloadlog", true) || !strcmp(tmp, "changemode", true))
			{
				if(!strcmp(tmp, "reloadbans", true))
				{
					SendClientMessage(playerid, 0xFFFFFFFF, " ");
					SendClientMessage(playerid, 0xFFFFFFFF, "Ban list");
					SendClientMessage(playerid, 0xFFFFFFFF, "--------");
					SendClientMessage(playerid, 0xFFFFFFFF, " Loaded: samp.ban");
					SendClientMessage(playerid, 0xFFFFFFFF, " ");
				}
				else if(!strcmp(tmp, "reloadlog", true))
				{
					SendClientMessage(playerid, 0xFFFFFFFF, " ");
					SendClientMessage(playerid, 0xFFFFFFFF, "--------");
					SendClientMessage(playerid, 0xFFFFFFFF, "Reloaded log file: \"server_log.txt\".");
					SendClientMessage(playerid, 0xFFFFFFFF, "--------");
				}
				else if(!strcmp(tmp, "changemode", true))
				{
					cmd = strtok(cmdtext, idx);
					if(strlen(cmd))
					{
						format(tmp, sizeof(tmp), "changemode %s", cmd);
					}
				}
				SendRconCommand(tmp);
			}
			else if(!strcmp(tmp, "kick", true))
			{
				cmd = strtok(cmdtext, idx);
				if(strlen(cmd) && isNumeric(cmd))
				{
				    new player = strval(cmd);
					if(IsPlayerConnected(player))
					{
					    new playername[MAX_PLAYER_NAME], ip[20];
					    GetPlayerName(player, playername, sizeof(playername));
					    GetPlayerIp(player, ip, sizeof(ip));
						format(cmd, sizeof(cmd), "%s <#%d - %s> has been kicked.", playername, player, ip);
						SendClientMessage(playerid, 0xFFFFFF00, cmd);
						new npcstring[5]; if(IsPlayerNPC(player)) { npcstring = "npc:"; }
					    format(cmd, sizeof(cmd), "[%spart] %s has left the server (%d:2)", npcstring, playername, player);
						SendClientMessage(playerid, 0xFFFFFF00, cmd);
						format(cmd, sizeof(cmd), "kick %d", player);
						SendRconCommand(cmd);
		    		}
    			}
			}
			else if(!strcmp(tmp, "ban", true))
			{
				cmd = strtok(cmdtext, idx);
				if(strlen(cmd) && isNumeric(cmd))
				{
				    new player = strval(cmd);
					if(IsPlayerConnected(player))
					{
					    new playername[MAX_PLAYER_NAME], ip[20];
					    GetPlayerName(player, playername, sizeof(playername));
					    GetPlayerIp(player, ip, sizeof(ip));
						format(cmd, sizeof(cmd), "%s <#%d - %s> has been banned.", playername, player, ip);
						SendClientMessage(playerid, 0xFFFFFF00, cmd);
						new npcstring[5]; if(IsPlayerNPC(player)) { npcstring = "npc:"; }
					    format(cmd, sizeof(cmd), "[%spart] %s has left the server (%d:2)", npcstring, playername, player);
						SendClientMessage(playerid, 0xFFFFFF00, cmd);
						format(cmd, sizeof(cmd), "banip %d", player);
						SendRconCommand(cmd);
		    		}
    			}
			}
			else if(!strcmp(tmp, "say", true))
			{
				cmd = strtok(cmdtext, idx);
				if(strlen(cmd))
				{
					format(cmd, sizeof(cmd), "* Admin: %s", cmd);
					SendClientMessageToAll(0x2587CEFF, cmd);
    			}
			}
			else if(!strcmp(tmp, "players", true))
			{
			    new playername[MAX_PLAYER_NAME], ip[20], listentry[MAX_PLAYER_NAME+sizeof(ip)+12], first = 1;
				for(new i; i < MAX_PLAYERS; i++)
				{
				    if(IsPlayerConnected(i))
				    {
						if(first) { first = 0; SendClientMessage(playerid, 0xFFFFFFFF, "ID Name Ping IP"); }
						GetPlayerName(i, playername, sizeof(playername));
						GetPlayerIp(i, ip, sizeof(ip));
						format(listentry, sizeof(listentry), "%d %s %d %s", i, playername, GetPlayerPing(i), ip);
						SendClientMessage(playerid, 0xFFFFFFFF, listentry);
					}
				}
			}
			else if(!strcmp(tmp, "banip", true) || !strcmp(tmp, "unbanip", true))
			{
				cmd = strtok(cmdtext, idx);
				if(strlen(cmd))
				{
				    new ipparts[4][4];
				    split(cmd, ipparts, '.');
				    if(-1 < strval(ipparts[0]) < 256 && isNumeric(ipparts[0]) && -1 < strval(ipparts[1]) < 256 && isNumeric(ipparts[1]) && -1 < strval(ipparts[2]) < 256 && isNumeric(ipparts[2]) && -1 < strval(ipparts[3]) < 256 && isNumeric(ipparts[3]))
				    {
						if(!strcmp(tmp, "banip", true)) { format(cmd, sizeof(cmd), "banip %s", cmd); } else { format(cmd, sizeof(cmd), "unbanip %s", cmd); }
						SendRconCommand(cmd);
						if(!strcmp(tmp, "banip", true)) { format(cmd, sizeof(cmd), "IP %s has been banned.", cmd); SendClientMessage(playerid, 0xFFFFFF00, cmd); }
					}
    			}
			}
			// VARLIST SHIT
			else if(!strcmp(tmp, "announce", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
																			format(tmp, sizeof(tmp), "announce = %d  (bool)", GetServerVarAsBool("announce"));					SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "announce", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
																			format(tmp, sizeof(tmp), "announce = %d  (bool)", GetServerVarAsBool("announce"));					SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "bind", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("bind", tmp, sizeof(tmp));				format(tmp, sizeof(tmp), "bind = \"%s\"  (string, read-only)", tmp);							SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "filterscripts", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("filterscripts", tmp, sizeof(tmp)); 	format(tmp, sizeof(tmp), "filterscripts = \"%s\"  (string, read-only)", tmp);							SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "gamemode0", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("gamemode0", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "gamemode0 = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "gamemode1", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("gamemode1", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "gamemode1 = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "gamemode10", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("gamemode10", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "gamemode10 = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "gamemode11", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("gamemode11", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "gamemode11 = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "gamemode12", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("gamemode12", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "gamemode12 = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "gamemode13", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("gamemode13", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "gamemode13 = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "gamemode14", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("gamemode14", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "gamemode14 = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "gamemode15", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("gamemode15", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "gamemode15 = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "gamemode2", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("gamemode2", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "gamemode2 = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "gamemode3", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("gamemode3", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "gamemode3 = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "gamemode4", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("gamemode4", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "gamemode4 = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "gamemode5", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("gamemode5", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "gamemode5 = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "gamemode6", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("gamemode6", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "gamemode6 = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "gamemode7", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("gamemode7", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "gamemode7 = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "gamemode8", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("gamemode8", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "gamemode8 = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "gamemode9", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("gamemode9", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "gamemode9 = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "gamemodetext", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("gamemodetext", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "gamemodetext = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "gravity", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("gravity", tmp, sizeof(tmp));			format(tmp, sizeof(tmp), "gravity = \"%s\"  (string)", tmp);									SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "hostname", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("hostname", tmp, sizeof(tmp));			format(tmp, sizeof(tmp), "hostname = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "incar_rate", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
																			format(tmp, sizeof(tmp), "incar_rate = %d  (int, read-only)", GetServerVarAsInt("incar_rate"));		SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "lanmode", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
																			format(tmp, sizeof(tmp), "lanmode = %d  (bool)", GetServerVarAsBool("lanmode"));					SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "logqueries", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
																			format(tmp, sizeof(tmp), "logqueries = %d  (bool)", GetServerVarAsBool("logqueries"));					SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "logtimeformat", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("logtimeformat", tmp, sizeof(tmp)); strrep(tmp, "%", "%%");	format(tmp, sizeof(tmp), "logtimeformat = \"%s\"  (string, read-only)", tmp);	SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "mapname", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("mapname", tmp, sizeof(tmp));			format(tmp, sizeof(tmp), "mapname = \"%s\"  (string)", tmp);									SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "maxnpc", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
																			format(tmp, sizeof(tmp), "maxnpc = %d  (int)", GetServerVarAsInt("maxnpc"));						SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "maxplayers", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
																			format(tmp, sizeof(tmp), "maxplayers = %d  (int, read-only)", GetServerVarAsInt("maxplayers"));		SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "myriad", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
																			format(tmp, sizeof(tmp), "myriad = %d  (bool)", GetServerVarAsBool("myriad"));						SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "nosign", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("nosign", tmp, sizeof(tmp));			format(tmp, sizeof(tmp), "nosign = \"%s\"  (string, read-only)", tmp);							SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "onfoot_rate", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
																			format(tmp, sizeof(tmp), "onfoot_rate = %d  (int, read-only)", GetServerVarAsInt("onfoot_rate"));		SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "output", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
																			format(tmp, sizeof(tmp), "output = %d  (bool)", GetServerVarAsBool("output"));						SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "password", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); format(tmp, sizeof(tmp), "Setting server password to: \"%s\"", cmd); SendClientMessage(playerid, 0xFFFFFFFF, tmp); return 1; }
				GetServerVarAsString("password", tmp, sizeof(tmp));			format(tmp, sizeof(tmp), "password = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "plugins", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("plugins", tmp, sizeof(tmp));			format(tmp, sizeof(tmp), "plugins = \"%s\"  (string, read-only)", tmp);							SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "port", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
																			format(tmp, sizeof(tmp), "port = %d  (int, read-only)", GetServerVarAsInt("port"));				SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "query", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
																			format(tmp, sizeof(tmp), "query = %d  (bool)", GetServerVarAsBool("query"));						SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "rcon", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
																			format(tmp, sizeof(tmp), "rcon = %d  (bool)", GetServerVarAsBool("rcon"));						SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "rcon_password", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("rcon_password", tmp, sizeof(tmp));	format(tmp, sizeof(tmp), "rcon_password = \"%s\"  (string)", tmp);										SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "sleep", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
																			format(tmp, sizeof(tmp), "sleep = %d  (int)", GetServerVarAsInt("sleep"));						SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "stream_distance", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
																			format(tmp, sizeof(tmp), "stream_distance = %d  (float)", GetServerVarAsInt("stream_distance"));	SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "stream_rate", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
																			format(tmp, sizeof(tmp), "stream_rate = %d  (int)", GetServerVarAsInt("stream_rate"));					SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "timestamp", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
																			format(tmp, sizeof(tmp), "timestamp = %d  (bool)", GetServerVarAsBool("timestamp"));					SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "version", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("version", tmp, sizeof(tmp));			format(tmp, sizeof(tmp), "version = \"%s\"  (string, read-only)", tmp);						SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "weapon_rate", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
																			format(tmp, sizeof(tmp), "weapon_rate = %d  (int, read-only)", GetServerVarAsInt("weapon_rate"));		SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "weather", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("weather", tmp, sizeof(tmp));			format(tmp, sizeof(tmp), "weather = \"%s\"  (string)", tmp);									SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "weburl", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("weburl", tmp, sizeof(tmp));			format(tmp, sizeof(tmp), "weburl = \"%s\"  (string)", tmp);									SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
			else if(!strcmp(tmp, "worldtime", true))
			{
			    cmd = strtok(cmdtext, idx); if(strlen(cmd)) { format(tmp, sizeof(tmp), "%s %s", tmp, cmd); SendRconCommand(tmp); return 1; }
				GetServerVarAsString("worldtime", tmp, sizeof(tmp));		format(tmp, sizeof(tmp), "worldtime = \"%s\"  (string)", tmp);									SendClientMessage(playerid, 0xFFFFFFFF, tmp);
			}
		}
		return 1;
	}
	return 0;
}

stock isNumeric(const string[])
{
	new length=strlen(string);
	if(length==0) return false;
	for(new i = 0; i < length; i++)
	{
		if((string[i] > '9' || string[i] < '0' && string[i]!='-' && string[i]!='+') /*Not a number,'+' or '-'*/
		|| (string[i]=='-' && i!=0)/* A '-' but not at first.*/
		|| (string[i]=='+' && i!=0)/* A '+' but not at first.*/
		)
		{
			return false;
		}
	}
	if(length==1 && (string[0]=='-' || string[0]=='+')) { return false; }
	return true;
}

stock strrep(string[], searchstring[], replacestring[], limit = 256) // replaces strings. By Meta
{
	new pos, posend;
	for(new i; i < limit; i++)
	{
		pos = strfind(string, searchstring, false, posend);
		if(pos == -1)
		{
		    if(!limit) { return 0; }
		    else { return 1; }
		}
	 	strdel(string, pos, pos+strlen(searchstring));
	 	strins(string, replacestring, pos, strlen(string));
	 	posend = pos+strlen(replacestring);
 	}
	return 1;
}

stock strtok(const string[], &index)
{
    new length = strlen(string);
    while ((index < length) && (string[index] <= ' '))
    {
        index++;
    }
    new offset = index;
    new result[128];
    while ((index < length) && (string[index] > ' ') && ((index - offset) < (sizeof(result) - 1)))
    {
        result[index - offset] = string[index];
        index++;
    }
    result[index - offset] = EOS;
    return result;
}

stock split(const strsrc[], strdest[][], delimiter)
{
    new i, li;
    new aNum;
    new len;
    while(i <= strlen(strsrc))
	{
    	if(strsrc[i] == delimiter || i == strlen(strsrc))
		{
            len = strmid(strdest[aNum], strsrc, li, i, 128);
            strdest[aNum][len] = 0;
            li = i+1;
            aNum++;
        }
        i++;
    }
    return 1;
}

forward MMF_RCONX(version[15]);
public MMF_RCONX(version[15])
{
	if(strcmp(VERSION, version, true) && strlen(version))
	{
	    return 2;
	}
	return 1;
}

forward IsPlayerAdminX(playerid);
public IsPlayerAdminX(playerid) { return GetPVarInt(playerid, "RCONExLogin"); }

/*	Call in Script: #define IsPlayerAdmin(%0) CallRemoteFunction("IsPlayerAdminX", "d", %0) */

/* ========== EOF ==========*/
